from pyDA.FixedAlgebra import *

FTps.setGlobalTruncOrder(5)
x = FTps.makeVariable(0)
px = FTps.makeVariable(1)
y = FTps.makeVariable(2)
z = FTps.makeVariable(4)

#print ( FTps.getGlobalTruncOrder() )


f = FTps(0, 4, 5)

f = 0.5 + x * x + 1.0 + 2.0 * y - x * y - z

g = FTps(0, 4, 5)

g = px + z

#print ( f )



#print ( f * g )


vec = FVector(3)

#print ( vec )

#print ( f.evaluate(vec) )

#print ( g.evaluate(vec) )

m = FVps(0, 4, 5)

m[0] = g
m[1] = 2.0 * g
m[2] = f + g
m[3] = f * g
m[4] = f * f + g
m[5] = 1.0 * x

#print ( m )


#print ( PoissonBracket(f, g, 4) )

#print ( f * f )

#print ( ExpMap(f*f, 4) )


#print ( len(a) )

#print ( Physics.pi )


print ( f )

h = f.gradient()

print ( h )

h = f.derivative(0)

print ( h )

h = f.integral(0, 5)

print ( h )
