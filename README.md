# PAM2 FS2022
This is the repository for the course Particle Accelerator Physics and Modelling 2 in FS 2022.

## pyAcceLEGOrator
All classes and functions are provided in the `AcceLEGOrator` folder. The required packagess are `numpy`, `scipy`, `matplotlib` and `jupyter notebook`. It's recommended to create a virtual enviroment. You can find detailed instructions [here](https://gitlab.ethz.ch/lisic/pam2-fs2022/-/blob/main/pyAcceLEGOrator_install.pdf).

## pyDA
During this course we'll be using *pyDA* -- the python binding of a C++ Differential Algebra package. The [pybind11 project](http://pybind11.readthedocs.io/en/stable/) is used for the binding.

### Installation
1. Use an ETH network or connect to [the ETH VPN]().
2. Log in to Euler
```bash
$ ssh your-nethz-id@euler.ethz.ch
Password: your-nethz-password
```
3. Clone the repository
```bash
$ git clone https://gitlab.ethz.ch/lisic/pam2-fs2022.git
$ cd pam2-fs2022
```
4. Load necessary modules and check
```bash
$ module load gcc/6.3.0 python/3.7.1 cmake/3.5.2
$ module list
Currently Loaded Modulefiles:
1) new                                4) python/3.7.1
2) gcc/6.3.0                          5) cmake/3.5.2(3.5)
3) openblas/0.2.13_seq(default:seq)
```
Make sure `gcc/6.3.0` is the only gcc module in the list. If there is any other version, run `module unload gcc/x.x.x` to unload it.

**Note** Check your own version of modules by `module avail gcc`, `module avail python` etc. and use the latest version. You need to modify Line 67 in `install.sh` accordingly.

5. Install
```bash
$ . install.sh
```
6. Check installation
```bash
$ python exercises/test.py
# (some outputs)
```
7. Environment setting

In the installation script, the `$PYTHONPATH` variable and the required modules are hard-coded into the `~/.bashrc` file, so you don't need to load the modules and set the path every time when you log in. You can logout of Euler and login again, then run
```bash
$ module list
```
and
```bash
$ python pam2-fs2022/exercises/test.py
```
to check if you get the same output as before.

## Jupyter notebook on Euler
### Installation
Please refer to [this page](https://scicomp.ethz.ch/wiki/Accessing_the_clusters#SSH_keys) to set up SSH key for Euler. You should be able to access Euler by `ssh your-nethz-id@euler.ethz.ch` without password.

Then refer to [this page](https://scicomp.ethz.ch/wiki/Jupyter_on_Euler_and_Leonhard_Open) to set up the connection from your local computer to Jupyterhub on Euler.

### Running
```bash
$ cd xxx/Jupyter-on-Euler-or-Leonhard-Open # where start_jupyter_nb.sh is located
xxx/Jupyter-on-Euler-or-Leonhard-Open$ ./start_jupyter_nb.sh -u your-nethz-id -n 4 -W 04:00 -m 2048
```
This means that you will start a session in your home directory on Euler, using 4 cores, lasting 4 hours long, and with memory limit of 2048 MB.
