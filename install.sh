#!/bin/bash

REPO_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# `which python3` -m pip install pytest

# path to the DA-python package folder
export DA_PACKAGE_DIR=${REPO_DIR}/Src/DA-python-package
# path to the pybind11 package folder
export PY_PACKAGE_DIR=${REPO_DIR}/Src/pybind11-package
# where to build
export BUILD_DIR=${REPO_DIR}/Build
# where to install the pybind11 and da library
export PREFIX=${REPO_DIR}/Install
# the number of cores used to compile
export NJOBS=2

# recipe for:
Ppy=pybind11
Vpy=2.4

# rewrite the previous build (if existing)
rm -rf "${BUILD_DIR}/$Ppy/build"

# configure
mkdir -p "${BUILD_DIR}/$Ppy/build" && cd "$_"
cmake \
    -DCMAKE_INSTALL_PREFIX:STRING="${PREFIX}" \
    -DCMAKE_BUILD_TYPE:STRING=Release \
   "${PY_PACKAGE_DIR}"

# compile & install
make -j ${NJOBS}
make install

# recipe for:
Pda=DA
Vda=python
PYTHON=ON

# rewrite the previous build (if existing)
rm -rf "${BUILD_DIR}/$Pda/build"

# configure
mkdir -p "${BUILD_DIR}/$Pda/build" && cd "$_"
cmake \
    -D CMAKE_INSTALL_PREFIX:STRING="${PREFIX}" \
    -D CMAKE_BUILD_TYPE:STRING=Release \
    -D ENABLE_PYTHON:BOOL=${PYTHON} \
    -DPYTHON_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  \
    -DPYTHON_LIBRARY=$(python3 -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))") \
    -D CMAKE_CXX_FLAGS=" -w -Wno-error=deprecated-declarations -Wno-error=unused-value" \
    "${DA_PACKAGE_DIR}"

# compile & install
make -j ${NJOBS}
make install

if [[ "${PYTHON}" == "ON" ]]; then
    cp "${BUILD_DIR}/$Pda/build/src/"*.so "${PREFIX}/lib"
fi

cd ${REPO_DIR}

function ev_setting() {
  # Set environment variables on .bashrc
echo '
module load gcc/6.3.0 python/3.7.1
export PYTHONPATH='${PREFIX}'/lib:$PYTHONPATH' >> ~/.bashrc;

  # Reload current environment
  source ~/.bashrc
}

# Execute the function
ev_setting
