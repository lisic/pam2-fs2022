import pyDA.FixedAlgebra as fa

# ---------------------------------
# Generator of all elements' map.
# ---------------------------------
class MapGenerator:
    def __init__(self):
        pass

    def generateMap(self, H, order, length, *args):
        Hamiltonian = H(*args, order)
        # how to implement length inside?
        ...
        return ...

# after completing the function, change the filename back to MapGenerator.py
