/* Name of package */
#define PACKAGE "classic"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "andreas.adelmann@psi.ch"

/* Define to the full name of this package. */
#define PACKAGE_NAME "classic"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "classic 5.1.3"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "classic"

/* Define to the version of this package. */
#define PACKAGE_VERSION "5.1.3"

/* Version number of package */
#define VERSION "5.1.3"
