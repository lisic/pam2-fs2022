include_directories (
  ${CMAKE_CURRENT_SOURCE_DIR}
  )

set (_HDRS
    pyPhysics.h
)

install (FILES ${_HDRS} DESTINATION "${CMAKE_INSTALL_PREFIX}/include/pyDA/Physics")
