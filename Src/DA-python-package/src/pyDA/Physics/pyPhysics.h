#ifndef PY_DA_PHYSICS_H
#define PY_DA_PHYSICS_H

#include <pybind11/pybind11.h>

#include "Physics/Physics.h"

void initPhysics(pybind11::module& m) {
    
    auto physics = m.def_submodule("Physics");
    
    physics.attr("pi") = Physics::pi;
    physics.attr("two_pi") = Physics::two_pi;
    physics.attr("c") = Physics::c;
    physics.attr("mu_0") = Physics::mu_0;
    physics.attr("epsilon_0") = Physics::epsilon_0;
    physics.attr("q_e") = Physics::q_e;
    physics.attr("PMASS") = Physics::PMASS;
    physics.attr("EMASS") = Physics::EMASS;
    physics.attr("PCHARGE") = Physics::PCHARGE;
}

#endif
