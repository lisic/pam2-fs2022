#ifndef PY_DA_EXCEPTION_H
#define PY_DA_EXCEPTION_H

#include <pybind11/pybind11.h>

#include "Utilities/ClassicException.h"

void initUtilities(pybind11::module& m) {
    
    auto utilities = m.def_submodule("Utilities");
    
    static pybind11::exception<ClassicException> ex_classic(utilities, "ClassicException");
    
    pybind11::register_exception_translator([](std::exception_ptr p) {
        try {
            if (p) std::rethrow_exception(p);
        } catch (const ClassicException &e) {
            ex_classic(e.what().c_str());
        }
    });
}
    
#endif
