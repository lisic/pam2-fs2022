#ifndef PY_DA_ARRAY_1D_H
#define PY_DA_ARRAY_1D_H

#include <pybind11/pybind11.h>

#include "Algebra/Array1D.h"

void initArray1D(pybind11::module& m) {
    namespace py = pybind11;
    
    py::class_< Array1D<double> >(m, "Array1D")
        .def(py::init<int>())
        .def(py::init<>())
        .def("size", &Array1D<double>::size)
        .def("resize", &Array1D<double>::resize)
        .def("__len__", [](const Array1D<double> &a) { return a.size(); })
        .def("__iter__", [](Array1D<double> &a) {
            return py::make_iterator(a.begin(), a.end());
        }, py::keep_alive<0, 1>())
        .def("__setitem__", [](Array1D<double> &a, int i, double v) { a[i] = v; })
        .def("__getitem__", [](Array1D<double> &a, int i) { return a[i]; });
}

#endif
