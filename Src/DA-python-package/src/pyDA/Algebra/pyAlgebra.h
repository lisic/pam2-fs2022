#ifndef PY_DA_ALGEBRA_H
#define PY_DA_ALGEBRA_H

#include "pyDA/Algebra/pyArray1D.h"

void initAlgebra(pybind11::module& m) {
    
    auto algebra = m.def_submodule("Algebra");
    
    initArray1D(algebra);
}

#endif
