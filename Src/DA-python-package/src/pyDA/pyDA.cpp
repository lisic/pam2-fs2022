#include "pyDA/Physics/pyPhysics.h"
#include "pyDA/Algebra/pyAlgebra.h"
#include "pyDA/FixedAlgebra/pyFixedAlgebra.h"
#include "pyDA/Utilities/pyUtilities.h"

PYBIND11_MODULE(pyDA, m)
{
    initPhysics(m);
    
    initAlgebra(m);
    
    initFixedAlgebra(m);
    
    initUtilities(m);
}
