#ifndef PY_DA_FTPS_MATH_H
#define PY_DA_FTPS_MATH_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFTpsMath(pybind11::module& m) {
    namespace py = pybind11;
    
    using pyda::size_type;
    using pyda::value_type;
    using pyda::FTps_t;
    using pyda::dim;
    
    m.def("pow", &pow<value_type, 2*dim>);
    
    m.def("sqrt", &sqrt<value_type, 2*dim>);
    
    m.def("sin", &sin<value_type, 2*dim>);
    
    m.def("cos", &cos<value_type, 2*dim>);
    
    m.def("tan", &tan<value_type, 2*dim>);
    
    m.def("cot", &cot<value_type, 2*dim>);
    
    m.def("sec", &sec<value_type, 2*dim>);
    
    m.def("csc", &csc<value_type, 2*dim>);
    
    m.def("exp", &exp<value_type, 2*dim>);
    
    m.def("log", &log<value_type, 2*dim>);
    
    m.def("sinh", &sinh<value_type, 2*dim>);
    
    m.def("cosh", &cosh<value_type, 2*dim>);
    
    m.def("tanh", &tanh<value_type, 2*dim>);
    
    m.def("coth", &coth<value_type, 2*dim>);
    
    m.def("sech", &sech<value_type, 2*dim>);
    
    m.def("csch", &csch<value_type, 2*dim>);
    
    m.def("erf", &erf<value_type, 2*dim>);
    
    m.def("erfc", &erfc<value_type, 2*dim>);
}

#endif
