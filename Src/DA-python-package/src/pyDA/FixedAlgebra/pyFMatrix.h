#ifndef PY_DA_FMatrix_H
#define PY_DA_FMatrix_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFMatrix(pybind11::module& m) {
    namespace py = pybind11;
    using pyda::value_type;
    using pyda::FMatrix_t;
    using pyda::FArray2D_t;

    py::class_< FMatrix_t , FArray2D_t > (m, "FMatrix")
        .def(py::init<>())
        .def(py::init<const value_type&>())
        .def(py::self *= value_type())
        .def(py::self /= value_type())
        .def(py::self += py::self)
        #ifdef __clang__
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wself-assign-overloaded"
        #endif
        .def(py::self -= py::self);
        #ifdef __clang__
        #pragma GCC diagnostic pop
        #endif
}

#endif
