#ifndef PY_DA_FIXED_ALGEBRA_CORE_H
#define PY_DA_FIXED_ALGEBRA_CORE_H

#include <pybind11/iostream.h>
#include <pybind11/operators.h>

#include "FixedAlgebra/FVector.h"
#include "FixedAlgebra/FArray1D.h"
#include "FixedAlgebra/FArray2D.h"
#include "FixedAlgebra/FMatrix.h"
#include "FixedAlgebra/FTps.h"
#include "FixedAlgebra/FTpsMath.h"
#include "FixedAlgebra/FVps.h"

namespace pyda {
    const int dim = 3;
    
    typedef int size_type;
    typedef double value_type;
    typedef FVector<value_type, 2*dim> FVector_t;
    typedef FArray1D<value_type, 2*dim> FArray1D_t;
    typedef FArray2D<value_type, 2*dim, 2*dim> FArray2D_t;
    typedef FMatrix<value_type, 2*dim, 2*dim> FMatrix_t;
    typedef FTps<value_type, 2*dim> FTps_t;
    typedef FVps<value_type, 2*dim> FVps_t;
}

#endif
