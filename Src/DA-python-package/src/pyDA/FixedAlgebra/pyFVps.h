#ifndef PY_DA_FVPS_H
#define PY_DA_FVPS_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFVps(pybind11::module& m) {
    namespace py = pybind11;

    using pyda::size_type;
    using pyda::value_type;
    using pyda::FMatrix_t;
    using pyda::FVps_t;
    using pyda::FVector_t;
    using pyda::FTps_t;
    using pyda::dim;

    py::class_< FVps_t > (m, "FVps")
        .def(py::init<>())
        .def(py::init<size_type, size_type, size_type>())
        .def(py::init<const FVector_t >())
        .def(py::init<const FVps_t >())
        .def("setMinOrder", &FVps_t::setMinOrder)
        .def("getMinOrder", &FVps_t::getMinOrder)
        .def("setMaxOrder", &FVps_t::setMaxOrder)
        .def("getMaxOrder", &FVps_t::getMaxOrder)
        .def("setTruncOrder", &FVps_t::setTruncOrder)
        .def("getTruncOrder", &FVps_t::getTruncOrder)
        .def("truncate", &FVps_t::truncate)
        .def("__setitem__", [](FVps_t &v, size_type i, const FTps_t& f) { v[i] = f; })
        .def("__getitem__", [](FVps_t &v, size_type i) { return v[i]; })
        .def("setComponent", &FVps_t::setComponent)
        .def(py::self * FVector_t())
        .def(py::self * py::self)
        .def(py::self * value_type())
        .def("linearTerms", py::overload_cast<>(&FVps_t::linearTerms, py::const_))
        .def("__str__", [](const FVps_t& v) {
            py::scoped_ostream_redirect stream(
                std::cout,
                py::module::import("sys").attr("stdout"));
            v.put(std::cout);
            return "";
        });

    m.def("PoissonBracket", py::overload_cast<const FTps_t &,
                                              const FTps_t &,
                                              size_type>(&PoissonBracket<value_type, 2*dim>));

    m.def("ExpMap", py::overload_cast<const FTps_t&, size_type>(&ExpMap<value_type, 2*dim>));
//         py::arg("trunc") = FTps_t::EXACT
//     );
}

#endif
