#ifndef PY_DA_FVector_H
#define PY_DA_FVector_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFVector(pybind11::module& m) {
    namespace py = pybind11;
    using pyda::value_type;
    using pyda::FVector_t;
    using pyda::FArray1D_t;

    py::class_< FVector_t , FArray1D_t > (m, "FVector")
        .def(py::init<>())
        .def(py::init<const value_type&>())
        .def(py::self *= value_type())
        .def(py::self /= value_type())
        .def(py::self += py::self)
        #ifdef __clang__
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wself-assign-overloaded"
        #endif
        .def(py::self -= py::self);
        #ifdef __clang__
        #pragma GCC diagnostic pop
        #endif
}

#endif
