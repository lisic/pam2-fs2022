#ifndef PY_DA_FARRAY_1D_H
#define PY_DA_FARRAY_1D_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFArray1D(pybind11::module& m) {
    namespace py = pybind11;
    
    using pyda::FArray1D_t;
    using pyda::value_type;
    
    py::class_< FArray1D_t > (m, "FArray1D")
        .def(py::init<>())
        .def(py::init<const value_type&>())
        .def(py::init<const FArray1D_t&>())
        .def("size", &FArray1D_t::size)
        .def("__setitem__", [](FArray1D_t &a, int i, double v) { a[i] = v; })
        .def("__getitem__", [](FArray1D_t &a, int i) { return a[i]; })
        .def("__len__", [](const FArray1D_t &a) { return a.size(); })
        .def("__str__", [](const FArray1D_t& f) {
            py::scoped_ostream_redirect stream(
                std::cout,
                py::module::import("sys").attr("stdout"));
            std::cout << f;
            return "";
        });
}

#endif
