#ifndef PY_DA_FIXED_ALGEBRA_H
#define PY_DA_FIXED_ALGEBRA_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

#include "pyDA/FixedAlgebra/pyFArray1D.h"
#include "pyDA/FixedAlgebra/pyFArray2D.h"
#include "pyDA/FixedAlgebra/pyFMatrix.h"
#include "pyDA/FixedAlgebra/pyFVector.h"
#include "pyDA/FixedAlgebra/pyFTps.h"
#include "pyDA/FixedAlgebra/pyFTpsMath.h"
#include "pyDA/FixedAlgebra/pyFVps.h"

void initFixedAlgebra(pybind11::module& m) {
    
    auto falgebra = m.def_submodule("FixedAlgebra");
    
    pybind11::add_ostream_redirect(falgebra, "std::ostream");
    
    initFArray1D(falgebra);
    initFArray2D(falgebra);
    initFMatrix(falgebra);
    initFTps(falgebra);
    initFTpsMath(falgebra);
    initFVector(falgebra);
    initFVps(falgebra);
}

#endif
