include_directories (
  ${CMAKE_CURRENT_SOURCE_DIR}
  )

set (_HDRS
    pyFixedAlgebraCore.h
    pyFixedAlgebra.h
    pyFArray1D.h
    pyFArray2D.h
    pyFMatrix.h
    pyFTps.h
    pyFTpsMath.h
    pyFVector.h
    pyFVps.h
)

install (FILES ${_HDRS} DESTINATION "${CMAKE_INSTALL_PREFIX}/include/pyDA/FixedAlgebra")
