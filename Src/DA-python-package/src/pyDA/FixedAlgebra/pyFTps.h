#ifndef PY_DA_FTPS_H
#define PY_DA_FTPS_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFTps(pybind11::module& m) {
    namespace py = pybind11;

    using pyda::size_type;
    using pyda::value_type;
    using pyda::FTps_t;

    py::class_< FTps_t > (m, "FTps")
        .def(py::init<>())
        .def(py::init<const FTps_t&>())
        .def(py::init<size_type, size_type, size_type>())
        .def(py::init<size_type>())
        .def("setMinOrder", &FTps_t::setMinOrder,
             "Set minimum order.\nIf necessary, this function will insert zeroes "
             "or modify the maximum order.\nIt will not modify the truncation order.")
        .def("getMinOrder", &FTps_t::getMinOrder)
        .def("setMaxOrder", &FTps_t::setMaxOrder)
        .def("getMaxOrder", &FTps_t::getMaxOrder)
        .def("setTruncOrder", &FTps_t::setTruncOrder)
        .def("getTruncOrder", &FTps_t::getTruncOrder)
        .def("truncate", &FTps_t::truncate)
        .def("evaluate", &FTps_t::evaluate)
        .def("inverse", &FTps_t::inverse)
        .def_static("makeVariable", &FTps_t::makeVariable)
        .def_static("makeVarPower", &FTps_t::makeVarPower)
        .def_static("setGlobalTruncOrder", &FTps_t::setGlobalTruncOrder)
        .def_static("getGlobalTruncOrder", &FTps_t::getGlobalTruncOrder)
        .def("gradient", &FTps_t::gradient)
        .def("derivative", &FTps_t::derivative)
        .def("integral", &FTps_t::integral)
        .def("taylor", &FTps_t::taylor)
        .def(py::self + py::self)
        .def(py::self + value_type())
        .def(value_type() + py::self)
        .def(py::self += py::self)
        .def(py::self += value_type())
        .def(py::self -= value_type())
        .def(py::self - py::self)
        .def(value_type() - py::self)
        .def(py::self - value_type())
        #ifdef __clang__
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wself-assign-overloaded"
        #endif
        .def(py::self -= py::self)
        #ifdef __clang__
        #pragma GCC diagnostic pop
        #endif
        .def(py::self *= py::self)
        .def(py::self *= value_type())
        .def(py::self * py::self)
        .def(value_type() * py::self)
        .def(py::self * value_type())
        .def(py::self /= value_type())
        .def(py::self / value_type())
        .def(-py::self)
        .def("__str__", [](const FTps_t& f) {
            py::scoped_ostream_redirect stream(
                std::cout,
                py::module::import("sys").attr("stdout"));
            f.put(std::cout);
            return "";
        });
}

#endif
