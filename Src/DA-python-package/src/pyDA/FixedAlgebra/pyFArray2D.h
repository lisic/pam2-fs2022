#ifndef PY_DA_FARRAY_2D_H
#define PY_DA_FARRAY_2D_H

#include "pyDA/FixedAlgebra/pyFixedAlgebraCore.h"

void initFArray2D(pybind11::module& m) {
    namespace py = pybind11;

    using pyda::FArray2D_t;
    using pyda::value_type;

    py::class_< FArray2D_t > (m, "FArray2D")
        .def(py::init<>())
        .def(py::init<const value_type&>())
        .def(py::init<const FArray2D_t&>())
        .def("size", &FArray2D_t::size)
        .def("putRow", &FArray2D_t::putRow)
        .def("__len__", [](const FArray2D_t &a) { return a.size(); })
        .def("__str__", [](const FArray2D_t& f) {
            py::scoped_ostream_redirect stream(
                std::cout,
                py::module::import("sys").attr("stdout"));
            std::cout << f;
            return "";
        });
}

#endif
